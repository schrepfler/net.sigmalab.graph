Minimalist graph library allowing evaluation of expressions on an expression tree. 
Based on the Composite pattern. Value types based on the jscience library, thus supporting evaluation of operations on Real, Complex, Text.. anything extending ValueType! 
Would be nice to add support boolean arithmetics, operation order and proper support for arity. (hint: fork, and add support for it =). 
Also, there might be a better way to implement Operations and also generic functions... API proposals? Also, what if function finishes outside of the domain... Implements a simple 
Visitor pattern as well.
Enjoy, improve, commit back!

[![Build Status](https://drone.io/bitbucket.org/schrepfler/net.sigmalab.graph/status.png)](https://drone.io/bitbucket.org/schrepfler/net.sigmalab.graph/latest)