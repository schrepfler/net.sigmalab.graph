package net.sigmalab.graph;

import java.util.logging.Logger;

public class NodeVisitor implements INodeVisitor {

   private static final Logger log = Logger.getAnonymousLogger();

   public void visit(Expression<?> expression) {
      log.info(expression.toString());
   }

   public void visit(ValueNode<?> valueNode) {
      log.info(valueNode.toString());
   }

}
