package net.sigmalab.graph;


public interface INodeVisitor {
	
	void visit(Expression<?> expression);
	void visit(ValueNode<?> valueNode);

}
