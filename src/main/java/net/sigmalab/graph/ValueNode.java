package net.sigmalab.graph;

import javolution.lang.ValueType;

public class ValueNode<T extends ValueType> implements IVisitableNode<T> {
	
	private ValueType value;

	public ValueNode(T value) {
		this.value = value;
	}

	@SuppressWarnings("unchecked")
	public T evaluate() {
		return (T)value;
	}

	@Override
	public String toString() {
		return "ValueNode [value=" + value + "]";
	}

	public void accept(INodeVisitor visitor) {
		visitor.visit(this);
	}
	
}
