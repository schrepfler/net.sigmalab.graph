package net.sigmalab.graph;

import javolution.lang.ValueType;

public interface INode<T extends ValueType> {
	
	T evaluate();

}
