package net.sigmalab.graph;

import static net.sigmalab.graph.GraphUtils.valsToNodes;

import java.util.ArrayList;
import java.util.List;

import javolution.lang.ValueType;
import javolution.text.Text;

import org.jscience.mathematics.structure.GroupAdditive;
import org.jscience.mathematics.structure.GroupMultiplicative;

public class Expression<T extends ValueType> implements IVisitableNode<T> {

    @Override
    public String toString() {
        return "Expression [opCode=" + opCode + ", operands=" + operands + "] evals to: " + this.evaluate().toString();
    }

    public enum Op {
        Add, Multiply, Subtract, Divide, Concat;
    };

    private List<INode<T>> operands = new ArrayList<INode<T>>();
    private Op opCode;

    @SuppressWarnings("unchecked")
    public static <T extends ValueType> Expression<T> Add(T... values) {
        return new Expression<T>(Op.Add, valsToNodes(values).toArray(new INode[values.length]));
    }

    @SuppressWarnings("unchecked")
    public static <T extends ValueType> Expression<T> Multiply(T... values) {
        return new Expression<T>(Op.Multiply, valsToNodes(values).toArray(new INode[values.length]));
    }

    @SuppressWarnings("unchecked")
    public static <T extends ValueType> Expression<T> Subtract(T... values) {
        return new Expression<T>(Op.Subtract, valsToNodes(values).toArray(new INode[values.length]));
    }

    @SuppressWarnings("unchecked")
    public static <T extends ValueType> Expression<T> Divide(T... values) {
        return new Expression<T>(Op.Divide, valsToNodes(values).toArray(new INode[values.length]));
    }

    @SuppressWarnings("unchecked")
    public static <T extends ValueType> Expression<T> Concat(T... values) {
        return new Expression<T>(Op.Concat, valsToNodes(values).toArray(new INode[values.length]));
    }

    /**
     * This constructor will generate an expression by inserting
     * 
     * @param opCode
     *            which determines which operation the expression needs to
     *            evaluate and
     * @param operands
     *            which determines against which operands the operation needs to
     *            be evaluated
     */
    public Expression(Op opCode, INode<T>... operands) {
        this.operands.clear();
        for (INode<T> operand : operands) {
            this.operands.add(operand);
        }

        this.opCode = opCode;
    }

    public Expression(Op opCode, T... operands) {
        this.operands.clear();
        for (int i = 0; i < operands.length; i++) {
            this.operands.add(GraphUtils.valToNode(operands[i]));
        }

        this.opCode = opCode;
    }

    public T evaluate() {

        assertOperandsInStructure();

        ValueType firstOperand = operands.get(0).evaluate();
        ValueType result = firstOperand;

        switch (opCode) {
        // Additive group operations
        case Add:
            for (int i = 1; i < operands.size(); i++) {
                INode<T> node = operands.get(i);
                result = ((GroupAdditive<ValueType>) node.evaluate()).plus(result);
            }
            break;

        case Subtract:
            for (int i = 1; i < operands.size(); i++) {
                INode<? extends ValueType> node = operands.get(i);
                result = ((GroupAdditive<ValueType>) ((GroupAdditive<ValueType>) node.evaluate()).opposite()).plus(result);
            }
            break;
        // Multiplicative group operations
        case Multiply:
            for (int i = 1; i < operands.size(); i++) {
                INode<? extends ValueType> node = operands.get(i);
                result = ((GroupMultiplicative<ValueType>) node.evaluate()).times(result);
            }
            break;

        case Divide:
            for (int i = 1; i < operands.size(); i++) {
                INode<? extends ValueType> node = operands.get(i);
                result = ((GroupMultiplicative<ValueType>) ((GroupMultiplicative<ValueType>) node.evaluate()).inverse()).times(result);
            }
            break;

        // String operations
        case Concat:
            for (int i = 1; i < operands.size(); i++) {
                INode<? extends ValueType> node = operands.get(i);
                result = ((Text) result).concat((Text) node.evaluate());
            }
            break;

        default:
            throw new UnsupportedOperationException("Operation not supported.");
        }

        return (T) result;
    }

    private void assertOperandsInStructure() {
        switch (opCode) {
        case Add:
            for (INode<? extends ValueType> node : operands) {
                if (node.evaluate() instanceof GroupAdditive<?>) {

                } else
                    throw new UnsupportedOperationException("Not additive group.");
            }
            break;
        case Divide:
            for (INode<? extends ValueType> node : operands) {
                if (node.evaluate() instanceof GroupMultiplicative<?>) {

                } else
                    throw new UnsupportedOperationException("Not multiplicative group.");
            }
            break;
        case Multiply:
            for (INode<? extends ValueType> node : operands) {
                if (node.evaluate() instanceof GroupMultiplicative<?>) {

                } else
                    throw new UnsupportedOperationException("Not multiplicative group.");
            }
            break;
        case Subtract:
            for (INode<? extends ValueType> node : operands) {
                if (node.evaluate() instanceof GroupAdditive<?>) {

                } else
                    throw new UnsupportedOperationException("Not additive group.");
            }
            break;
        case Concat:
            for (INode<? extends ValueType> node : operands) {
                if (node.evaluate() instanceof Text) {

                } else
                    throw new UnsupportedOperationException("Not text.");
            }
            break;

        default:
            break;
        }
    }

    public void accept(INodeVisitor visitor) {
        visitor.visit(this);
        for (INode<T> node : operands) {
            if (node instanceof IVisitableNode) {
                ((IVisitableNode) node).accept(visitor);
            } else {
                throw new UnsupportedOperationException("Node not visitable");
            }
        }
    }

}
