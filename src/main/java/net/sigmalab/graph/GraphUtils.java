package net.sigmalab.graph;

import java.util.ArrayList;
import java.util.List;

import javolution.lang.ValueType;

public class GraphUtils {
	
	public static <T extends ValueType> List<ValueNode<T>> valsToNodes(T... values) {
		List<ValueNode<T>> valueNodes = new ArrayList<ValueNode<T>>(values.length);
		for (int i = 0; i < values.length; i++) {
			valueNodes.add(new ValueNode<T>(values[i]));
		}
		return valueNodes;
	}
	
	public static <T extends ValueType> ValueNode<T> valToNode(T value){
		return new ValueNode<T>(value);
	}

}
