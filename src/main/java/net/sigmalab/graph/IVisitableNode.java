package net.sigmalab.graph;

import javolution.lang.ValueType;

public interface IVisitableNode<T extends ValueType> extends INode<T> {
   
   void accept(INodeVisitor visitor);

}
