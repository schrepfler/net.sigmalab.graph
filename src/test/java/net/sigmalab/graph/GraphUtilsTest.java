package net.sigmalab.graph;

import java.util.List;

import org.jscience.mathematics.number.Real;
import org.junit.Test;

import static org.junit.Assert.*;
import static net.sigmalab.graph.GraphUtils.valsToNodes;

public class GraphUtilsTest {

	@Test
	public void testValsToNodes() throws Exception {
		List<ValueNode<Real>> nodes = valsToNodes(Real.valueOf(6d), Real.ZERO, Real.valueOf(3d));
		assertEquals(Real.valueOf(6d), nodes.get(0).evaluate());
		assertEquals(Real.ZERO, nodes.get(1).evaluate());
		assertEquals(Real.valueOf(3d), nodes.get(2).evaluate());
	}

}
