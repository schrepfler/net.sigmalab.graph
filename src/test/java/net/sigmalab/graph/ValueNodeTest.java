package net.sigmalab.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.logging.Logger;

import javolution.text.Text;
import net.sigmalab.graph.Expression.Op;

import org.jscience.mathematics.number.Complex;
import org.jscience.mathematics.number.Real;
import org.junit.Before;
import org.junit.Test;

public class ValueNodeTest {

    protected static final Logger log = Logger.getGlobal();

    protected ValueNode<Real> five = new ValueNode<Real>(Real.valueOf(5d));
    protected ValueNode<Real> three = new ValueNode<Real>(Real.valueOf(3d));
    protected ValueNode<Real> six = new ValueNode<Real>(Real.valueOf(6d));
    protected ValueNode<Real> zero = new ValueNode<Real>(Real.valueOf(0d));

    protected Expression<Real> times = new Expression<Real>(Op.Multiply, five, three);

    protected Expression<Real> plus = new Expression<Real>(Op.Add, times, six);

    protected Expression<Real> eighteen = Expression.Multiply(Real.valueOf(6d), Real.valueOf(3d));

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testReal() {
        assertEquals(Real.valueOf(5d), five.evaluate());
        log.info(five.toString());
        assertFalse(Real.valueOf(6d).equals(five.evaluate()));
        log.info(six.toString());

        assertEquals(Real.valueOf(18d).doubleValue(), eighteen.evaluate().doubleValue(), 0d);

        assertTrue(Real.valueOf(21d).approximates(plus.evaluate()));

        Expression<Real> division = new Expression<Real>(Op.Divide, six, three);

        assertTrue(Real.valueOf(2d).approximates(division.evaluate()));

        Expression<Real> subtraction = new Expression<Real>(Op.Subtract, six, five);
        log.info(subtraction.toString());
        Expression<Real> divisionWithZero = new Expression<Real>(Op.Divide, six, zero);
        Real resultOfDivisionWithZero = divisionWithZero.evaluate();
        assertTrue(resultOfDivisionWithZero.isNaN());
        log.info(divisionWithZero.toString());
    }

    @Test
    public void testText() {
        // String tests
        ValueNode<Text> someText = new ValueNode<Text>(Text.valueOf("someText"));
        ValueNode<Text> evenMoreText = new ValueNode<Text>(Text.valueOf("evenMoreText"));
        Expression<Text> concat = new Expression<Text>(Op.Concat, someText, evenMoreText);
        log.info(concat.toString());

        assertTrue(concat.evaluate().contentEquals("someTextevenMoreText"));
    }

    @Test
    public void testComplex() {
        // Complex tests

        ValueNode<Complex> complex1 = new ValueNode<Complex>(Complex.valueOf(2d, 2d));
        ValueNode<Complex> complex2 = new ValueNode<Complex>(Complex.valueOf(2d, 2d));

        Expression<Complex> addComplex = new Expression<Complex>(Op.Add, complex1, complex2);

        assertTrue(Complex.valueOf(4d, 4d).equals(addComplex.evaluate(), 0d));
        log.info(addComplex.toString());
    }

    @Test
    public void testVisitor() {
        // Tests visitor

        INodeVisitor visitor = new NodeVisitor();
        plus.accept(visitor);

    }

}
